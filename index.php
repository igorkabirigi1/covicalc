<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Covicalc - home</title>
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/bootstrap.min.css">
    <script src="jquery-3.4.1.min.js"></script>

</head>
<body>
    <div class="container">
        <div class="row">
            <div class="col-sm-6 mt-3">
                <svg xmlns="http://www.w3.org/2000/svg" width="174" height="51" viewBox="0 0 174 51"><defs><style>.a{fill:#fff;font-size:42px;font-family:BebasNeueBold, Bebas Neue;}</style></defs><text class="a"><tspan x="0" y="38">C O V I C A L C</tspan></text></svg>
            </div>
            <div class="col-sm-6 d-flex justify-content-end mt-3">
                <button class="btn btn-default" id="contactbtn">CONTACT</button>
            </div>
        </div>
        <div class="row">
            <div class="col-sm-12">
                <p id="updatestxt">UPDATES</p>
                <p id="updtxt">Search a country</p>
            </div>
        
        
            <div class="mx-auto" id="formcountry">
                    <form action="" class="form-inline">
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">x</div>
                            </div>
                                
                            <select name="countryname" id="pay" class="form-control ">
                                
                            </select>
                        </div>
                        <div class="input-group">
                            <div class="input-group-prepend">
                                <div class="input-group-text">x</div>
                            </div>
                            <input type="date" class="form-control">
                        </div>
                        <button class="btn btn-default" id="contactbtn">SUBMIT</button>
                        
                    </form>
            </div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-2">

            </div>
            <div class="col-sm-8" id="displaycontentone">
                <p>2,188,881</p>
                <p>Cumulatively</p>
            </div>
            <div class="col-sm-2">

            </div>
        </div>
        <div class="row" id="displaytwo">
            <div class="col-sm-2">

            </div>
            <div class="col-sm-8" id="displaycontenttwoone">
                <div class="row">
                    <div class="col-sm-2">
                        <p>11,270</p>
                        <p>Tests</p>
                        <p>2,188,881</p>
                    </div>
                    <div class="col-sm-2">
                        <p>619</p>
                        <p>Positive cases</p>
                        <p>4254</p>
                    </div>
                    <div class="col-sm-2">
                        <p>20</p>
                        <p>Hospitalized</p>
                        <p>19</p>
                    </div>
                    <div class="col-sm-2">
                        <p>19</p>
                        <p>Recovered</p>
                        <p>88,881</p>
                    </div>
                    <div class="col-sm-2">
                        <p>9</p>
                        <p>Deaths</p>
                        <p>920</p>
                    </div>
                    <div class="col-sm-2">
                        <p>48,660</p>
                        <p>vaccinated</p>
                        <p>729,180</p>
                    </div>
                </div>
            </div>
            <div class="col-sm-2">

            </div>
        </div>

        <div class="row" id="displaytwo">
            <div class="col-sm-12">
                <p id="pcontinentstxt">PER CONTINENTS</p>
            </div>

            <div class="col-sm-12" id="percontinent">
                <div class="row">
                    <div class="col-sm-5 m-3" id="continentsone">
                        <div class="col-sm-12" id="continent">
                            <div class="row">
                                <div class="col-sm-6" id="cone">
                                    <p id="ctext">AFRICA</p>
                                    <p id="ctexttwo">11,270</p>
                                    <p id="ctextthree">New cases</p>
                                    <p id="ctextfour">All cases: 22,188,881</p>
                                </div>
                                <div class="col-sm-6" id="ctwo">
                                    <p id="death-number">619</p>
                                    <p id="death-text">New deaths</p>
                                    <p id="total-deaths">Total deaths: 4,254</p><hr>
                                    <p id="death-number">12,955</p>
                                    <p id="death-text">Newly recovered</p>
                                    <p id="total-deaths">Total recovered: 12,254,254</p><hr>
                                    <p id="death-number">1,878,564</p>
                                    <p id="death-text">New vaccinated</p>
                                    <p id="total-deaths">Total vaccinated: 14,784,254</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>

                    <div class="col-sm-5 m-3" id="continentsone">
                        <div class="col-sm-12" id="continent">
                            <div class="row" >
                                <div class="col-sm-6" id="cone">
                                    <p id="ctext">AFRICA</p>
                                    <p id="ctexttwo">11,270</p>
                                    <p id="ctextthree">New cases</p>
                                    <p id="ctextfour">All cases: 22,188,881</p>
                                </div>
                                <div class="col-sm-6" id="ctwo">
                                    <p id="death-number">619</p>
                                    <p id="death-text">New deaths</p>
                                    <p id="total-deaths">Total deaths: 4,254</p><hr>
                                    <p id="death-number">12,955</p>
                                    <p id="death-text">Newly recovered</p>
                                    <p id="total-deaths">Total recovered: 12,254,254</p><hr>
                                    <p id="death-number">1,878,564</p>
                                    <p id="death-text">New vaccinated</p>
                                    <p id="total-deaths">Total vaccinated: 14,784,254</p>
                                    
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-sm-12" style="height: 9vh;"></div>
        </div>
        
    </div>

    <div class="container-fluid">
        <div class="row" id="partthree">
            <div class="col-sm-6" id="partthreeone">

            </div>
            <div class="col-sm-6" id="partthreetwo">
                <p id="three-text">Kabirigi Igor</p>
                <p id="three-text-two">Website design, Website Development!</p>
                <p id="three-text-three">20 August 2021</p>
            </div>
        </div>
    </div>

    <div class="container-fluid">
        <div class="row" id="contact">
            <div class="col-sm-12" id="contact-header">
                <p>REACH ME</p>
            </div>
            <div class="col-sm-12" id="contact-q">
                <p>Email</p>
            </div>
            <div class="col-sm-12" id="contact-a">
                <p>[ igorkabirigi@gmail.com ]</p>
            </div>
            <div class="col-sm-12" id="contact-q">
                <p>Phone</p>
            </div>
            <div class="col-sm-12" id="contact-a">
                <p>[ 0785080573 ]</p>
            </div>
            <div class="col-sm-12" id="contact-q">
                <p>Profile</p>
            </div>
            <div class="col-sm-12" id="contact-a">
                <p>[ No profile ]</p>
            </div>

            <div class="col-sm-12" style="height: 12vh;"></div>
        </div>
    </div>
    <div class="container-fluid">
        <div class="row" id="footer">
            <div class="col-sm-6">
                <p>Developed by<span> [ Kabirigi Ishimwe Igor ]</span></p>
            </div>
            <div class="col-sm-6 d-flex justify-content-end">
                <p>Designed by<span> [ Awesomity lab ]</span></p>
            </div>
        </div>
    </div>
</body>
</html>


<script>
var settings = {
  "url": "https://corona.lmao.ninja/v2/countries?yesterday&sort",
  "method": "GET",
  "timeout": 0,
};

    $.ajax(settings).done(function (response) {
    console.log(response);

        $.each(response, function(index,value){
        

            var option = "<option value='0'>countries</option>";
            
            var namelength = response.length;

            for (let i = 0;  i < namelength ; i++) {
                $.each(value, function(){
                    var name = value.country;
                    option += "<option value='"+name+"'>"+name+"</option>";
                });
            }
            $("#pay").append(option);

        });
    });        
</script>